package genetic;

public interface IIndividual {
    float getFitness();

    float fitness();

    IIndividual generate(final int... size);

    IIndividual crossover(final IIndividual B, final float uniform);

    IIndividual mutate(final float mutation_freq);
}
