package main;

public class Config {
    public static int NODES_MAX_POP;
    public static int NODES_MULTI;
    public static int NODES_ELITISM;
    public static float NODES_CROSSOVER_FREQ;
    public static float NODES_CROSSOVER_UNIFORM;
    public static float NODES_MUTATION_FREQ;
    public static int EDGES_MAX_POP;
    public static int EDGES_MULTI;
    public static int EDGES_ELITISM;
    public static float EDGES_CROSSOVER_FREQ;
    public static float EDGES_CROSSOVER_UNIFORM;
    public static float EDGES_MUTATION_FREQ;

    public static void setConfig(final int config_id) {
        switch (config_id) {
            case 1:
                NODES_MAX_POP = 110;
                NODES_MULTI = 2;
                NODES_ELITISM = 3;
                NODES_CROSSOVER_FREQ = 0.95f;
                NODES_CROSSOVER_UNIFORM = 0.50f;
                NODES_MUTATION_FREQ = 0.01f;
                EDGES_MAX_POP = 200;
                EDGES_MULTI = 1;
                EDGES_ELITISM = 3;
                EDGES_CROSSOVER_FREQ = 0.95f;
                EDGES_CROSSOVER_UNIFORM = 0.50f;
                EDGES_MUTATION_FREQ = 0.02f;
                break;
            case 2:
                NODES_MAX_POP = 150;
                NODES_MULTI = 2;
                NODES_ELITISM = 3;
                NODES_CROSSOVER_FREQ = 0.95f;
                NODES_CROSSOVER_UNIFORM = 0.50f;
                NODES_MUTATION_FREQ = 0.01f;
                EDGES_MAX_POP = 200;
                EDGES_MULTI = 1;
                EDGES_ELITISM = 3;
                EDGES_CROSSOVER_FREQ = 0.95f;
                EDGES_CROSSOVER_UNIFORM = 0.50f;
                EDGES_MUTATION_FREQ = 0.04f;
                break;
            case 3:
                NODES_MAX_POP = 100;
                NODES_MULTI = 3;
                NODES_ELITISM = 3;
                NODES_CROSSOVER_FREQ = 0.95f;
                NODES_CROSSOVER_UNIFORM = 0.50f;
                NODES_MUTATION_FREQ = 0.01f;
                EDGES_MAX_POP = 200;
                EDGES_MULTI = 1;
                EDGES_ELITISM = 3;
                EDGES_CROSSOVER_FREQ = 0.95f;
                EDGES_CROSSOVER_UNIFORM = 0.50f;
                EDGES_MUTATION_FREQ = 0.01f;
                break;
            case 4:
                NODES_MAX_POP = 100;
                NODES_MULTI = 3;
                NODES_ELITISM = 3;
                NODES_CROSSOVER_FREQ = 0.95f;
                NODES_CROSSOVER_UNIFORM = 0.50f;
                NODES_MUTATION_FREQ = 0.01f;
                EDGES_MAX_POP = 100;
                EDGES_MULTI = 1;
                EDGES_ELITISM = 3;
                EDGES_CROSSOVER_FREQ = 0.95f;
                EDGES_CROSSOVER_UNIFORM = 0.50f;
                EDGES_MUTATION_FREQ = 0.05f;
                break;
            default:
                NODES_MAX_POP = 100;
                NODES_MULTI = 1;
                NODES_ELITISM = 3;
                NODES_CROSSOVER_FREQ = 0.95f;
                NODES_CROSSOVER_UNIFORM = 0.50f;
                NODES_MUTATION_FREQ = 0.01f;
                EDGES_MAX_POP = 100;
                EDGES_MULTI = 1;
                EDGES_ELITISM = 3;
                EDGES_CROSSOVER_FREQ = 0.95f;
                EDGES_CROSSOVER_UNIFORM = 0.50f;
                EDGES_MUTATION_FREQ = 0.01f;
                break;
        }
    }
}
