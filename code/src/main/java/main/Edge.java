package main;

import engine.Engine;
import engine.interfaces.IEdge;
import engine.interfaces.INode;
import engine.results.EdgeResult;
import engine.results.Result;
import genetic.IIndividual;
import impl.GraphEdge;
import impl.Solution;
import utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

public class Edge implements IIndividual {
    public static Engine engine;
    public static INode[] nodes;
    private final IEdge[] edges;
    private float fitness;

    public Edge() {
        this.edges = null;
        this.fitness = -1;
    }

    public Edge(IEdge[] edges) {
        this.edges = edges;
        this.fitness = -1;
    }

    /**
     * Compare to edges
     *
     * @param e1 the first edge
     * @param e2 the second edge
     * @return true if equal false if not
     */
    public static boolean compare(IEdge e1, IEdge e2) {
        return e1.getStart().getX() == e2.getStart().getX() &&
                e1.getStart().getY() == e2.getStart().getY() &&
                e1.getEnd().getX() == e2.getEnd().getX() &&
                e1.getEnd().getY() == e2.getEnd().getY() ||
                /* part 2 */
                e1.getStart().getX() == e2.getEnd().getX() &&
                        e1.getStart().getY() == e2.getEnd().getY() &&
                        e1.getEnd().getY() == e2.getStart().getY() &&
                        e1.getEnd().getX() == e2.getStart().getX();
    }

    /**
     * Generate a edge
     *
     * @param primary if is initial generation
     * @return the generated edge
     */
    private IEdge generateEdge(int n_id, boolean primary) {
        INode n1;
        INode n2;
        do {
            n1 = nodes[Utils.generate_int(nodes.length)];
            if (!primary) {
                for (INode n : nodes) {
                    if (!inUse(n)) {
                        n1 = n;
                        break;
                    }
                }
            }
            n2 = nodes[Utils.generate_int(nodes.length)];
        } while (Node.compare(n1, n2));
        String label = "" + (char) (65 + n_id);
        return new GraphEdge(n1, n2, Utils.generate_int(1, 3), label);
    }

    private boolean inUse(INode n) {
        if (edges != null) {
            for (IEdge e : edges) {
                if (e.getStart() == n || e.getEnd() == n) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get percentage of correct nodes
     *
     * @return percentage of nodes
     */
    public float correctEdges() {
        Result res = getResult();
        int i = 0;
        for (EdgeResult er : res.getEdgeResults())
            if (er.isBoth_nodes_ok() && er.isIs_weight_ok()) i++;
        return (float) ((i  * 100) / res.getEdgeResults().size());
    }

    /**
     * Check to see if a edge exists
     *
     * @param edge the edge to see if exists
     * @return true if exists false if not
     */
    private boolean exists(IEdge edge) {
        if (edges != null && edges.length > 0) {
            for (IEdge iEdge : edges) {
                if (iEdge == null) return false;
                if (compare(edge, iEdge)) return true;
            }
        }
        return false;
    }

    /**
     * Check to see if there are repeating edges
     *
     * @return true if is repeating false if not
     */
    private boolean repeat() {
        if (edges != null && edges.length > 1) {
            for (int i = 0; i < edges.length; i++)
                for (int j = 0; j < edges.length; j++)
                    if (i != j && compare(this.edges[i], this.edges[j]))
                        return true;
        }
        return false;
    }

    /**
     * @return A list of edges
     */
    public IEdge[] getEdges() {
        return this.edges;
    }

    public Result getResult() {
        return engine.testSolution(getSolution());
    }

    public Solution getSolution() {
        ArrayList<INode> l_nodes = new ArrayList<>();
        Collections.addAll(l_nodes, nodes);

        ArrayList<IEdge> edges = new ArrayList<>();
        if (this.edges != null) Collections.addAll(edges, this.edges);
        return new Solution(l_nodes, edges, engine.getVirusConfiguration());
    }

    @Override
    public float getFitness() {
        return this.fitness;
    }

    @Override
    public float fitness() {
        float fitness = 0;
        Result res = getResult();
        if (edges == null) return 0;
        if (!repeat()) {
            for (EdgeResult er : res.getEdgeResults()) {
                if (er.isOne_node_ok()) fitness++;
                if (er.isIs_weight_ok()) fitness++;
                if (er.isBoth_nodes_ok()) fitness++;
            }
        }
        this.fitness = (fitness / edges.length) / 3;
        return this.fitness;
    }

    @Override
    public IIndividual generate(final int... size) {
        int final_size = size[0];
        IEdge[] edges = new IEdge[final_size];
        for (int i = 0; i < final_size; i++)
            do {
                edges[i] = generateEdge(i, true);
            } while (exists(edges[i]));
        return new Edge(edges);
    }

    @Override
    public IIndividual crossover(final IIndividual B, final float uniform) {
        if (edges == null) return null;
        Edge gb = (Edge) B;
        IEdge[] e = new IEdge[gb.getEdges().length];
        for (int i = 0; i < gb.getEdges().length; i++) {
            e[i] = gb.getEdges()[i];
            if (Math.random() < uniform)
                e[i] = this.edges[i];
        }
        return new Edge(e);
    }

    @Override
    public IIndividual mutate(final float mutation_freq) {
        if (edges == null) return null;
        for (int i = 0; i < this.edges.length; i++) {
            if (Math.random() < mutation_freq) {
                IEdge edge;
                do {
                    edge = generateEdge(this.edges.length + i, false);
                } while (exists(edge));
                this.edges[i] = edge;
            }
        }
        return new Edge(this.edges);
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("\nEdges: \n");
        Arrays.stream(Objects.requireNonNull(this.edges)).map(IEdge::toString).forEach(v -> b.append(v).append("\n"));
        return b + "\n";
    }
}
