package main;

import engine.Engine;
import engine.exceptions.ElementNotFoundException;
import engine.exceptions.VirusDoesNotExistException;
import engine.exceptions.VisualizationNotFoundException;
import genetic.GeneticAlgorithm;
import genetic.Population;
import utils.Record;
import utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static final boolean USE_VISUALIZER = true;
    private static boolean GET_NUMBER_OK = false;
    private static final String VIZ_ID = "viz";
    private static GeneticAlgorithm ga;
    private static int generation;
    private static List<float[]> f = null;

    public static void main(String[] args) {
        try {
            Scanner sn = new Scanner(System.in);
            int op, round_numbers;
            String s;
            boolean save = false;
            do {
                System.out.println("1 - Run virus 1");
                System.out.println("2 - Run virus 2");
                System.out.println("3 - Run virus 3");
                System.out.println("4 - Run virus 4");
                System.out.println("5 - Run rounds");
                System.out.println("0 - Exit\n");
                System.out.print("Enter Option: ");
                op = sn.nextInt();

                if (op != 0) {
                    System.out.print("\nSave to file: ");
                    sn.reset();
                    s = sn.next();
                    if (s.contains("y") || s.contains("Y")) {
                       save = true;
                    }
                }
                GET_NUMBER_OK = save;

                switch (op) {
                    case 1:
                        if (save) {
                            saveVirus(1, 1);
                        } else {
                            runVirus(1, 1, 1);
                        }
                        break;
                    case 2:
                        if (save) {
                            saveVirus(2, 2);
                        } else {
                            runVirus(2, 2, 1);
                        }
                        break;
                    case 3:
                        if (save) {
                            saveVirus(3, 3);
                        } else {
                            runVirus(3, 3, 1);
                        }
                        break;
                    case 4:
                        if (save) {
                            saveVirus(4, 4);
                        } else {
                            runVirus(4, 4, 1);
                        }
                        break;
                    case 5:
                        GET_NUMBER_OK = false;
                        System.out.print("\nEnter number of rounds: ");
                        round_numbers = sn.nextInt();
                        runRounds(round_numbers);
                        break;
                }
            } while (op != 0);
        } catch (VisualizationNotFoundException | VirusDoesNotExistException | ElementNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void saveVirus(final int virus_id, final int config_id) throws VisualizationNotFoundException, VirusDoesNotExistException, ElementNotFoundException {
        Record r = runVirus(virus_id, config_id, 1);
        if (f != null) {
            Utils.saveToFile(r, virus_id);
            f.clear();
        } else {
            System.out.println("It didn't save");
        }
    }

    private static void runRounds(int round_numbers) throws VisualizationNotFoundException, VirusDoesNotExistException, ElementNotFoundException {
        StringBuilder virus_stats = new StringBuilder();
        for (int virus_id = 1; virus_id < 5; virus_id++) {
            for (int conf_id = 1; conf_id < 5; conf_id++) {
                List<Record> data = new ArrayList<>();
                for (int round = 1; round <= round_numbers; round++) {
                    data.add(runVirus(virus_id, conf_id, round));
                }
                Utils.saveToFile(data, virus_id);
                virus_stats.append("v_").append(virus_id)
                        .append("_cnf_").append(conf_id)
                        .append(Record.min_stats(data)).append("\n");
                virus_stats.append("v_").append(virus_id)
                        .append("_cnf_").append(conf_id)
                        .append(Record.medium_stats(data)).append("\n");
                virus_stats.append("v_").append(virus_id)
                        .append("_cnf_").append(conf_id)
                        .append(Record.max_stats(data)).append("\n\n");
            }
        }
        System.out.println(virus_stats);
    }

    /**
     * Run a virus and return record
     *
     * @param virus_id the virus to run using its config
     * @param round    the round
     * @return a record of the virus
     */
    public static Record runVirus(final int virus_id, int config_id, int round)
            throws VisualizationNotFoundException, VirusDoesNotExistException, ElementNotFoundException {

        long start = System.nanoTime();

        Config.setConfig(config_id);
        Engine eng = new Engine(virus_id);
        if (USE_VISUALIZER) eng.addVisualization(VIZ_ID);
        Node.engine = eng;
        Edge.engine = eng;

        ga = new GeneticAlgorithm(Config.NODES_ELITISM, Config.NODES_CROSSOVER_FREQ,
                Config.NODES_CROSSOVER_UNIFORM, Config.NODES_MUTATION_FREQ);

        generation = 0;

        Node n = runNodes();
        n.finalNodes();
        System.out.println("Found Nodes in Generation: -> " + generation);
        System.out.println(n.getResult().toString());

        ga = new GeneticAlgorithm(Config.EDGES_ELITISM, Config.EDGES_CROSSOVER_FREQ,
                Config.EDGES_CROSSOVER_UNIFORM, Config.EDGES_MUTATION_FREQ);

        Edge.nodes = n.getNodes();
        Edge e = runEdges();
        System.out.println("Found Edges and Nodes in Generation: -> " + generation);
        System.out.println(e.getResult().toString());

        double elapsedTime = (System.nanoTime() - start) / 1_000_000_000.0;
        System.out.printf("\nIt took %f TO END\n", elapsedTime);

        if (GET_NUMBER_OK) {
           return new Record(f);
        }
        return new Record(virus_id, config_id, round, e.getFitness(), generation, elapsedTime);
    }

    /**
     * Run Nodes problem
     *
     * @return the best node
     */
    private static Node runNodes() throws VisualizationNotFoundException {
        Node n = new Node();
        Population nodes_pop = Population.init_populations(Config.NODES_MAX_POP, n,
                Node.engine.getVirusConfiguration().getN_nodes(), Config.NODES_MULTI);

        runAlgo(nodes_pop, true);
        n = (Node) nodes_pop.getIndividuals().get(0);
        return n;
    }

    /**
     * Run Edges problem
     *
     * @return the best edges
     */
    private static Edge runEdges() throws VisualizationNotFoundException {
        Edge e = new Edge();
        Population edges_pop = Population.init_populations(Config.EDGES_MAX_POP, e,
                Edge.engine.getVirusConfiguration().getN_edges());

        runAlgo(edges_pop, false);
        e = (Edge) edges_pop.getIndividuals().get(0);
        return e;
    }

    /**
     * Generic algorithm runner
     *
     * @param pop   the population
     * @param nodes if is running the nodes or edges
     */
    private static void runAlgo(Population pop, boolean nodes) throws VisualizationNotFoundException {
        float fitness = Population.calc_fitness(pop);
        while (fitness < 1) {
            System.out.println("Generation (" + generation + ") Fitness -> " + fitness);
            ga.selection(pop);
            ga.crossover(pop);
            ga.mutation(pop);
            generation++;
            fitness = Population.calc_fitness(pop);
            Population.sort_population(pop);
            values(pop, nodes);
            update(pop, nodes);
        }
        Population.sort_population(pop);
        update(pop, nodes);
    }

    /**
     * Update the visualizer with the best individual
     *
     * @param pop   the population
     * @param nodes if is running the nodes or edges
     */
    private static void update(Population pop, boolean nodes) throws VisualizationNotFoundException {
        if (!USE_VISUALIZER) return;
        if (nodes) {
            Node n = (Node) pop.getIndividuals().get(0);
            Node.engine.updateVisualization(VIZ_ID, n.getSolution());
        } else {
            Edge e = (Edge) pop.getIndividuals().get(0);
            Edge.engine.updateVisualization(VIZ_ID, e.getSolution());
        }
    }

    /**
     * add percentage of correct edges
     *
     * @param pop   the population
     * @param nodes if is running the nodes or edges
     */
    private static void values(Population pop, boolean nodes) {
        if (!GET_NUMBER_OK) return;
        if (f == null) f = new ArrayList<>();
        float[] values;
        if (nodes) {
            Node n = (Node) pop.getIndividuals().get(0);
            values = new float[]{n.correctNodes(), 0f, n.getFitness(), generation};
        } else {
            Edge e = (Edge) pop.getIndividuals().get(0);
            values = new float[]{100f, e.correctEdges(), e.getFitness(), generation};
        }
        f.add(values);
    }
}