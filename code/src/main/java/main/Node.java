package main;

import engine.Engine;
import engine.interfaces.INode;
import engine.results.NodeResult;
import engine.results.Result;
import genetic.IIndividual;
import impl.GraphNode;
import impl.Solution;
import utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Node implements IIndividual {
    public static Engine engine;
    private static int MULTI;
    private static int NODES_SIZE;
    private INode[] nodes;
    private int x_min, y_min, x_max, y_max;
    private float fitness;

    public Node() {
        this.nodes = null;
        this.fitness = -1;
        initPositions();
    }

    public Node(INode[] nodes) {
        this.nodes = nodes;
        this.fitness = -1;
        initPositions();
    }

    /**
     * Compare to nodes
     *
     * @param n1 the first node
     * @param n2 the second node
     * @return true if equal false if not
     */
    public static boolean compare(INode n1, INode n2) {
        return n1.getX() == n2.getX() && n1.getY() == n2.getY();
    }

    private void initPositions() {
        this.x_min = engine.getVirusConfiguration().getX_origin();
        this.y_min = engine.getVirusConfiguration().getY_origin();
        this.x_max = x_min + engine.getVirusConfiguration().getWidth();
        this.y_max = y_min + engine.getVirusConfiguration().getHeigth();
    }

    /**
     * Generate a node
     *
     * @param n_id the id of the node
     * @return the generated node
     */
    private INode generateNode(int n_id) {
        GraphNode node;
        do {
            int x = Utils.generate_int(this.x_min, this.x_max);
            int y = Utils.generate_int(this.y_min, this.y_max);
            node = new GraphNode(Utils.generate_type(), n_id, x, y);
        } while (exists(node));
        return node;
    }

    /**
     * Check to see if a node exists
     *
     * @param node the node to see if exists
     * @return true if exists false if not
     */
    private boolean exists(GraphNode node) {
        if (node != null && nodes != null && nodes.length > 0) {
            for (INode iNode : nodes) {
                if (compare(node, iNode))
                    return true;
            }
        }
        return false;
    }

    /**
     * Check to see if there are repeating nodes
     *
     * @return true if is repeating false if not
     */
    private boolean repeat() {
        if (nodes != null && nodes.length > 0) {
            for (int i = 0; i < nodes.length; i++)
                for (int j = 0; j < nodes.length; j++)
                    if (i != j && compare(this.nodes[i], this.nodes[j]))
                        return true;
        }
        return false;
    }

    /**
     * @return A list of nodes
     */
    public INode[] getNodes() {
        return this.nodes;
    }

    /**
     * Get percentage of correct nodes
     *
     * @return percentage of nodes
     */
    public float correctNodes() {
        Result res = getResult();
        int i = 0;
        for (NodeResult nr : res.getNodeResults())
            if (nr.isOk()) i++;
        return (float) ((i * 100) / res.getNodeResults().size());
    }

    /**
     * Get list of correct nodes
     */
    public void finalNodes() {
        INode[] nodes = new INode[NODES_SIZE];
        Result res = getResult();
        int i = 0;
        for (NodeResult nr : res.getNodeResults())
            if (nr.isOk()) nodes[i++] = getById(nr.getId());
        this.nodes = nodes;
    }

    /**
     * Get node by id
     *
     * @param id the id of the node
     * @return the node or null no node found
     */
    private INode getById(int id) {
        for (INode node : nodes) {
            if (node.getLabel() == id) return node;
        }
        return null;
    }

    public Result getResult() {
        return engine.testSolution(getSolution());
    }

    public Solution getSolution() {
        ArrayList<INode> nodes = new ArrayList<>();
        Collections.addAll(nodes, this.nodes);
        return new Solution(nodes, new ArrayList<>(), engine.getVirusConfiguration());
    }

    @Override
    public float getFitness() {
        return this.fitness;
    }

    @Override
    public float fitness() {
        float fitness = 0;
        Result res = getResult();
        if (nodes == null) return 0;
        if (!repeat()) {
            for (NodeResult nr : res.getNodeResults())
                if (nr.isOk()) fitness++;
        }
        this.fitness = (fitness / nodes.length) * MULTI;
        return this.fitness;
    }

    @Override
    public IIndividual generate(final int... size) {
        NODES_SIZE = size[0];
        MULTI = size[1];
        int final_size = NODES_SIZE * MULTI;
        INode[] nodes = new INode[final_size];
        for (int i = 0; i < final_size; i++)
            nodes[i] = generateNode(i);
        return new Node(nodes);
    }

    @Override
    public IIndividual crossover(final IIndividual B, final float uniform) {
        if (nodes == null) return null;
        Node gb = (Node) B;
        INode[] n = new INode[gb.getNodes().length];
        for (int i = 0; i < gb.getNodes().length; i++) {
            n[i] = gb.getNodes()[i];
            if (Math.random() < uniform)
                n[i] = this.nodes[i];
        }
        return new Node(n);
    }

    @Override
    public IIndividual mutate(final float mutation_freq) {
        if (nodes == null) return null;
        for (int i = 0; i < this.nodes.length; i++) {
            if (Math.random() < mutation_freq)
                this.nodes[i] = generateNode(i);
        }
        return new Node(this.nodes);
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("\nNodes: \n");
        Arrays.stream(this.nodes).map(INode::toString).forEach(v -> {
            b.append(v).append("\n");
        });
        return b + "\n";
    }
}
