package utils;

import java.util.List;

public class Record {
    private int virusId, generation, configId, round;
    private float fitness;
    private double elapsedTime;
    private List<float[]> values;

    public Record(final int virusId,
                  final int configId,
                  final int round,
                  final float fitness, final int generation, final double elapsedTime) {

        this.configId = configId;
        this.round = round;
        this.virusId = virusId;
        this.fitness = fitness;
        this.generation = generation;
        this.elapsedTime = elapsedTime;
    }

    public Record(List<float[]> values) {
        this.values = values;
    }

    public static String medium_stats(List<Record> data) {
        float fitness = 0, generation = 0, elapsedTime = 0;
        for (Record r : data) {
            fitness += r.getFitness();
            generation += r.getGeneration();
            elapsedTime += r.getElapsedTime();
        }

        return " medium_fitness: " + (fitness / data.size())
                + " medium_generations: " + (generation / data.size())
                + " medium_elapsedTime: " + (elapsedTime / data.size());
    }

    public static String min_stats(List<Record> data) {
        float fitness = data.get(0).getFitness();
        int generation = data.get(0).getGeneration();
        double elapsedTime = data.get(0).getElapsedTime();
        for (Record r : data) {
            fitness = Math.min(fitness, r.getFitness());
            generation = Math.min(generation, r.getGeneration());
            elapsedTime = Math.min(elapsedTime, r.getElapsedTime());
        }

        return " min_fitness: " + fitness
                + " min_generations: " + generation
                + " min_elapsedTime: " + elapsedTime;
    }

    public static String max_stats(List<Record> data) {
        float fitness = data.get(0).getFitness();
        int generation = data.get(0).getGeneration();
        double elapsedTime = data.get(0).getElapsedTime();
        for (Record r : data) {
            fitness = Math.max(fitness, r.getFitness());
            generation = Math.max(generation, r.getGeneration());
            elapsedTime = Math.max(elapsedTime, r.getElapsedTime());
        }

        return " max_fitness: " + fitness
                + " max_generations: " + generation
                + " max_elapsedTime: " + elapsedTime;
    }

    public int getRound() {
        return round;
    }

    public int getConfigId() {
        return configId;
    }

    public int getVirusId() {
        return virusId;
    }

    public float getFitness() {
        return fitness;
    }

    public int getGeneration() {
        return generation;
    }

    public double getElapsedTime() {
        return elapsedTime;
    }

    public List<float[]> getValues() {
        return values;
    }
}

