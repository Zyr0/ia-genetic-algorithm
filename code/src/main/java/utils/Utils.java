package utils;

import engine.enums.NodeType;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Utils {
    public static final String SAVE_DIR = "./saves/";

    /**
     * Generate random number between 0(inclusive) and max(exclusive)
     *
     * @param max the max number to generate to
     */
    public static int generate_int(final int max) {
        Random r = new Random();
        return r.nextInt(max);
    }

    /**
     * Generate random number between min(inclusive) and max(inclusive)
     *
     * @param min the min number to generate to
     * @param max the max number to generate to
     */
    public static int generate_int(final int min, final int max) {
        Random r = new Random();
        return r.nextInt((max - min + 1)) + min;
    }

    /**
     * Generate random node type
     */
    public static NodeType generate_type() {
        return NodeType.values()[Utils.generate_int(NodeType.values().length)];
    }

    /**
     * Create a dir to save the files.
     */
    public static void saveToFile(List<Record> records, final int virus_id) {
        try {
            Path path = Paths.get(SAVE_DIR);
            if (!Files.isDirectory(path))
                Files.createDirectory(path);

            String filename = "virus_" + virus_id + ".csv";
            path = Paths.get(SAVE_DIR, filename);

            FileWriter writer = new FileWriter(path.toString(), true);
            writer.append("#, config_id, fitness, generation, elapsedTime\n");
            for (Record r : records) {
                writer.append(String.valueOf(r.getRound())).append(",")
                        .append(String.valueOf(r.getConfigId())).append(",")
                        .append(String.valueOf(r.getFitness())).append(",")
                        .append(String.valueOf(r.getGeneration())).append(",")
                        .append(String.valueOf(r.getElapsedTime())).append("\n");
            }
            writer.close();

        } catch (IOException e) {
            System.err.println("Failed to create dir.");
            e.printStackTrace();
        }
    }

    /**
     * Create a dir to save the file.
     */
    public static void saveToFile(final Record record, final int virus_id) {
        try {
            Path path = Paths.get(SAVE_DIR);
            if (!Files.isDirectory(path))
                Files.createDirectory(path);

            String filename = "values_virus_" + virus_id + ".csv";
            path = Paths.get(SAVE_DIR, filename);

            FileWriter writer = new FileWriter(path.toString(), true);
            writer.write("#, nodes, edges, fitness, generation\n");
            int i = 0;
            if (record.getValues() == null) { writer.close(); return; }
            for (int j = 0; j < record.getValues().size(); j++) {
                float[] f = record.getValues().get(j);
                writer.append(makeString(i, f));
                i++;
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Failed to create dir.");
            e.printStackTrace();
        }
    }

    private static String makeString(final int i, final float[] f) {
       return i + "," + f[0] + "," + f[1] + "," + f[2] + "," + f[3] + "\n";
    }
}
