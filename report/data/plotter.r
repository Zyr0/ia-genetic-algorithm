# Helper
################################
## https://r-coder.com/plot-r ##
################################

# BASIC PLOTS OF THE ROUNDS
virus1 <- read.csv("rounds_virus_1.csv");
plot(virus1$elapsedTime, virus1$generation, xlab="Elapsed Time (s)", ylab="Generation", main="Vírus 1 Rounds", col="blue");
lines(lowess(virus1$elapsedTime, virus1$generation), col = "orange", lwd = 3)
virus2 <- read.csv("rounds_virus_2.csv");
plot(virus2$elapsedTime, virus2$generation, xlab="Elapsed Time (s)", ylab="Generation", main="Vírus 2 Rounds", col="blue");
lines(lowess(virus2$elapsedTime, virus2$generation), col = "orange", lwd = 3)
virus3 <- read.csv("rounds_virus_3.csv");
plot(virus3$elapsedTime, virus3$generation, xlab="Elapsed Time (s)", ylab="Generation", main="Vírus 3 Rounds", col="blue");
lines(lowess(virus3$elapsedTime, virus3$generation), col = "orange", lwd = 3)
virus4 <- read.csv("rounds_virus_4.csv");
plot(virus4$elapsedTime, virus4$generation, xlab="Elapsed Time (s)", ylab="Generation", main="Vírus 4 Rounds", col="blue");
lines(lowess(virus4$elapsedTime, virus4$generation), col = "orange", lwd = 3)

# ROUND COMPARATOR COMPARE 1 with 2
plot(virus1$elapsedTime, virus1$generation, xlab="Elapsed Time (s)", ylab="Generation", main="Vírus 1 vs Vírus 2", col="blue");
points(virus2$elapsedTime, virus2$generation, col="red");
legend("bottomright", legend=c("Virus 1", "Virus 2"), lwd=3, col=c("blue", "red"))

# ROUND COMPARATOR COMPARE 3 with 4
v3ylim <- max(virus3$elapsedTime);
v3xlim <- max(virus3$generation);
plot(virus3$elapsedTime, virus3$generation, xlim=c(0, v3xlim), ylim=c(0, v3ylim), xlab="Elapsed Time (s)", ylab="Generation", main="Vírus 3 vs Vírus 4", col="blue");
points(virus4$elapsedTime, virus4$generation, col="red");
legend("bottomright", legend=c("Virus 4", "Virus 3"), lwd=3, col=c("blue", "red"))

# BASIC INFO
virus1_values <- read.csv("values_virus_1.csv")
plot(virus1_values$generation, virus1_values$fitness, ylim=c(0:1),
     xlab="Generation", ylab="Fitness", main="Vírus 1 AG over time", type="l", col="red");
lines(virus1_values$nodes / 100, lwd=1, lty=2, col="blue");
lines(virus1_values$edges / 100, lwd=1, lty=3);
legend("topleft", legend=c("Fitness", "nº nodes", "nº edges"), lwd=3, lty=c(1:3), col=c("red", "blue", "grey"))

virus2_values <- read.csv("values_virus_2.csv")
plot(virus2_values$generation, virus2_values$fitness, ylim=c(0:1),
     xlab="Generation", ylab="Fitness", main="Vírus 2 AG over time", type="l", col="red");
lines(virus2_values$nodes / 100, lwd=1, lty=2, col="blue");
lines(virus2_values$edges / 100, lwd=1, lty=3);
legend("bottomright", legend=c("Fitness", "%nodes", "%edges"), lwd=3, lty=c(1:3), col=c("red", "blue", "grey"))

virus3_values <- read.csv("values_virus_3.csv")
plot(virus3_values$generation, virus3_values$fitness, ylim=c(0:1),
     xlab="Generation", ylab="Fitness", main="Vírus 3 AG over time", type="l", col="red");
lines(virus3_values$nodes / 100, lwd=1, lty=2, col="blue");
lines(virus3_values$edges / 100, lwd=1, lty=3);
legend("bottomright", legend=c("Fitness", "%nodes", "%edges"), lwd=3, lty=c(1:3), col=c("red", "blue", "grey"))

virus4_values <- read.csv("values_virus_4.csv")
plot(virus4_values$generation, virus4_values$fitness, ylim=c(0:1),
     xlab="Generation", ylab="Fitness", main="Vírus 4 AG over time", type="l", col="red");
lines(virus4_values$nodes / 100, lwd=1, lty=2, col="blue");
lines(virus4_values$edges / 100, lwd=1, lty=3);
legend("bottomright", legend=c("Fitness", "%nodes", "%edges"), lwd=3, lty=c(1:3), col=c("red", "blue", "grey"))
